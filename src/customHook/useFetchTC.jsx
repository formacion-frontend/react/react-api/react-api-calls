import React, { useEffect, useState } from 'react';

export const useFetchTC = ({urlAPI,setStateData,setStateError=null,setStateLoading=null,errorMSG="detectado error"}) => {

 
    setStateLoading(true)

    const llamadaApi = async ()=>{
        try{
            const llamada =  await fetch(urlAPI)
            const datos = await llamada.json()
            setStateData(datos);
            setStateLoading(false)
        }
        catch(error){
            setStateError(errorMSG+"-"+error)
            setStateLoading(false)
            console.log(errorMSG,error);
        }
    }


};
