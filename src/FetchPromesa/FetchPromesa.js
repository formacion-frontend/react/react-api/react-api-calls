
import React, { useEffect, useState } from 'react';

export const FetchPromesa = () => {

    useEffect(() => {
    llamadaApi();
    }, []);
    
    const [contenidoFetch, setcontenidoFetch] = useState([]);

    const llamadaApi = async ()=>{
      
        await fetch('https://jsonplaceholder.typicode.com/users')
             .then((respuesta)=>{
                 respuesta.json()
                 .then((evento)=>{console.log(evento); setcontenidoFetch(evento)})
                 .catch(()=>{ console.log("detectado error Json"); })
                 
             })
             .catch((error)=>{
                 console.log("detectado error llamada",error);
             })
            
    }




  return(
         <ul>
             {contenidoFetch.map((obj)=>(<li key={obj.id}>{obj.name}</li>))}
         </ul>
         );
};
