
import React, { useEffect, useState } from 'react';

export const Fetch = () => {

    useEffect(() => {
    llamadaApi();
    }, []);
    
    const [contenidoFetch, setcontenidoFetch] = useState([]);

    const llamadaApi = async ()=>{
       const llamada =  await fetch('https://jsonplaceholder.typicode.com/users')
       const datos = await llamada.json()
        // console.log(datos);
        setcontenidoFetch(datos);   
    }
//---------------------------------------- OPCION 2
    // const llamadaApi = async ()=>{
    //     const llamada = await(await fetch('https://jsonplaceholder.typicode.com/users')).json()
    //      setcontenidoFetch(llamada);   
    //  }




  return(
        <ul>
             {contenidoFetch.map((obj)=>(<li key={obj.id}>{obj.name}</li>))}
         </ul>
            );

};
