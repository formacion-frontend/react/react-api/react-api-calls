import React, { useEffect, useState } from 'react';
import axios from 'axios';

export const AxiosBasico = () => {
 
    useEffect(() => {
        llamadaApi();
        }, []);
        
        const [contenidoFetch, setcontenidoFetch] = useState([]);
    
        const llamadaApi = async ()=>{
            const llamada =  await axios.get('https://jsonplaceholder.typicode.com/users')
            console.log("ejemplo",llamada);
            setcontenidoFetch(llamada.data);   
        }
//------------------------------------------------------- OPCION 2 PONIENDO EL DATA EN LA MISMA LINEA
        // const llamadaApi = async ()=>{
        //     const llamada =  await(await axios.get('https://jsonplaceholder.typicode.com/users')).data
        //     console.log("ejemplo",llamada);
        //     setcontenidoFetch(llamada);   
        // }
    
    
    
    
      return(
            <ul>
                 {contenidoFetch.map((obj)=>(<li key={obj.id}>{obj.name}</li>))}
             </ul>
                );
    
    };
    