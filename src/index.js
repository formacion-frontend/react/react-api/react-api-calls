import React from 'react';
import ReactDOM from 'react-dom';
import { AxiosBasico } from './Axios/AxiosBasico';
import { Fetch } from './FetchBasico/Fetch';
import { FetchPromesa } from './FetchPromesa/FetchPromesa';
import { FetchTryCatch } from './FetchTryCatch/FetchTryCatch';

ReactDOM.render(
  <React.StrictMode>
    <Fetch></Fetch>
    {/* <FetchTryCatch></FetchTryCatch> */}
    {/* <FetchPromesa></FetchPromesa> */}
    {/* <AxiosBasico></AxiosBasico> */}
  </React.StrictMode>,
  document.getElementById('root')
);