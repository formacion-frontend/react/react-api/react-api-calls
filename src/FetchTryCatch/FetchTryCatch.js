
import React, { useEffect, useState } from 'react';

export const FetchTryCatch = () => {

    useEffect(() => {
    llamadaApi();
    }, []);
    
    const [contenidoFetch, setcontenidoFetch] = useState([]);

    const llamadaApi = async ()=>{
        try{
            const llamada =  await fetch('https://jsonplaceholder.typicode.com/users')
            const datos = await llamada.json()
            setcontenidoFetch(datos);
        }
        catch(error){
            console.log("detectado error",error);
        }
    }




  return(
         <ul>
             {contenidoFetch.map((obj)=>(<li key={obj.id}>{obj.name}</li>))}
         </ul>
        );

};
